const app = require("./index");

const cors = require('cors');

// Configuração do CORS com origens permitidas
const corsOptions = {
  origin: 'http://localhost:5000/', // Substitua pela origem permitida
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  optionsSuccessStatus: 204,
};

app.use(cors(corsOptions));
app.listen(5000, () => {
  console.log('Servidor na porta 5000.');
  console.log('http://localhost:5000/showTables/');
  console.log('http://localhost:5000/tabelacomDesc1/');
  console.log('http://localhost:5000/tabelacomDesc2/');
  console.log('http://localhost:5000/tabelacomDesc3/');
  console.log('http://localhost:5000/tabelacomDesc4/');
  console.log('http://localhost:5000/tabelacomDesc5/');
  console.log('http://localhost:5000/tabelacomDesc6/');
});