const express = require('express');
const app = express();
const cors = require('cors');
const db = require('./db/connect');
// const testConnect = require('./db/testConnect')

class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.express.use(express.json());
    this.express.use(cors());
  }

  routes()
    {
        const rotas = require('./routes/rotas')
        this.express.use('/eventos/', rotas)
        function filtrarResposta(resposta) {
          
            if (!resposta || typeof resposta !== 'object') {
              return resposta;
            }
          
     
            const propriedadesParaRemover = ['_buf', '_clientEncoding', '_catalogLength', '_catalogStart', '_schemaLength', '_schemaStart', '_tableLength', '_tableStart', '_orgTableLength', '_orgTableStart', '_orgNameLength', '_orgNameStart', 'characterSet','encoding', 'name', 'columnLength', 'columnType', 'type', 'flags', 'decimals', '{}'               ];                                                                    
            propriedadesParaRemover.forEach((propriedade) => {
              if (Array.isArray(resposta)) {
                resposta = resposta.map((item) => {
                  delete item[propriedade];
                  return filtrarResposta(item);
                });
              } else {
                delete resposta[propriedade];
              }
            });
          
         
            return resposta;
          }
          
          
        
          this.express.get('/showTables/', (req, res) => {
            db.query('SHOW TABLES').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
         
          this.express.get('/tabelacomDesc1/', (req, res) => {
            db.query('DESC Usuario').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/tabelacomDesc2/', (req, res) => {
            db.query('DESC Compra').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/tabelacomDesc3/', (req, res) => {
            db.query('DESC Vendas').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/tabelacomDesc4/', (req, res) => {
            db.query('DESC Organizador').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/tabelacomDesc5/', (req, res) => {
            db.query('DESC Evento').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
          this.express.get('/tabelacomDesc6/', (req, res) => {
            db.query('DESC Ingresso').then((resultado) => {
              const resultadoFiltrado = filtrarResposta(resultado);
              res.status(200).json(resultadoFiltrado);
            }).catch((erro) => {
              res.status(200).json({ error: 'Não foi possível conectar a base dos dados SQL.', erro });
            });
          });
          
    }
}

module.exports = new AppController().express;
