const router = require("express").Router();
const connect = require("../db/connect");
const dbController = require("../controller/dbController");

router.get("/tables", dbController.getTables);

module.exports = router;
